# TestYourHTTP

test elixir, rust, java and go how fast they can process requests of a defined rest api.

## API:

```sh
GET    /const # get const string "abc...xzy1...9"
GET    /var   # get from a shared var
PUT    /var   # put body into shared var
DELETE /var   # empty shared var
```

## VALUES:

const       = `abcdefghijklmnopqrstuvwxyz123456789`  
var (start) = `start value`  
var (put)   = `0987654321_asdfghjklöä`  

## TEST:

one run:
 - 100x get const
 - 50x get var
 - 1x put var
 - 50x get var
 - 1x del var
 - 50x get var
