package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

var (
	constURL, _ = url.Parse("http://localhost:8080/const")
	varURL, _   = url.Parse("http://localhost:8080/var")
)

var getConstReq = &http.Request{
	Method: http.MethodGet,
	URL:    constURL,
}

var getVarReq = &http.Request{
	Method: http.MethodGet,
	URL:    constURL,
}

func main() {
	client := http.DefaultClient
	wg := sync.WaitGroup{}
	startTime := time.Now()

	// 100x get const
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			resp, _ := client.Do(getConstReq)
			_, _ = io.Copy(io.Discard, resp.Body)
		}()
	}
	wg.Wait()

	// 50x get var
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			resp, _ := client.Do(getConstReq)
			_, _ = io.Copy(io.Discard, resp.Body)
		}()
	}
	wg.Wait()

	// 1x put var
	resp, _ := client.Do(&http.Request{
		Method: http.MethodPut,
		URL:    constURL,
		Body:   io.NopCloser(strings.NewReader("0987654321_asdfghjklöä")),
	})
	_, _ = io.Copy(io.Discard, resp.Body)

	// 50x get var
	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			resp, _ := client.Do(getConstReq)
			_, _ = io.Copy(io.Discard, resp.Body)
		}()
	}
	wg.Wait()

	// 1x del var
	resp, _ = client.Do(&http.Request{
		Method: http.MethodDelete,
		URL:    constURL,
	})
	_, _ = io.Copy(io.Discard, resp.Body)

	// 50x get var
	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			resp, _ := client.Do(getConstReq)
			_, _ = io.Copy(io.Discard, resp.Body)
		}()
	}
	wg.Wait()

	fmt.Printf("Test took %v\n", time.Since(startTime))
}
